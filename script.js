let totalPrice = 0;

function updateTotalPrice() {
    document.getElementById('total').textContent = totalPrice.toFixed(2);
}

function increaseQuantity(itemId) {
    const quantityElement = document.getElementById(`quantity${itemId}`);
    const quantity = parseInt(quantityElement.textContent);
    quantityElement.textContent = quantity + 1;
    totalPrice += 10; // Prix par article (ajustez selon vos besoins)
    updateTotalPrice();
}

function decreaseQuantity(itemId) {
    const quantityElement = document.getElementById(`quantity${itemId}`);
    const quantity = parseInt(quantityElement.textContent);
    if (quantity > 1) {
        quantityElement.textContent = quantity - 1;
        totalPrice -= 10; // Prix par article (ajustez selon vos besoins)
        updateTotalPrice();
    }
}

function removeItem(itemId) {
    const quantityElement = document.getElementById(`quantity${itemId}`);
    const quantity = parseInt(quantityElement.textContent);
    totalPrice -= quantity * 10; // Retirer le prix total des articles supprimés
    updateTotalPrice();
    const itemElement = document.querySelector(`.item:nth-child(${itemId})`);
    itemElement.remove();
}

function toggleLike(itemId) {
    const likeButton = document.querySelector(`.like-btn:nth-child(${itemId})`);
    likeButton.classList.toggle('active');
}
